package com.massimoregoli.nodalessons;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.massimoregoli.nodalessons.database.CardInfo;

public class AddInfoActivity extends AppCompatActivity implements View.OnClickListener {
  Button btnOk, btnCancel;
  EditText etLabel, etInfo;
  String password;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_info);

    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      password = extras.getString("password");
    }
    btnOk = (Button)findViewById(R.id.btnOK);
    btnCancel = (Button)findViewById(R.id.btnCancel);
    etInfo = (EditText)findViewById(R.id.etInfo);
    etLabel = (EditText)findViewById(R.id.etLabel);

    btnOk.setOnClickListener(this);
    btnCancel.setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {
    if(view.getId() == btnOk.getId()) {
      if(inputIsOk()) {
        CardInfo si = new CardInfo(etLabel.getText().toString(),
                etInfo.getText().toString(), password);
        Intent intent = new Intent();
        intent.putExtra("info", si.getCSVInfo());
        setResult(RESULT_OK, intent);
        finish();
      }
    }
    if(view.getId() == btnCancel.getId()) {
      Intent intent = new Intent();
      setResult(RESULT_CANCELED, intent);
      finish();
    }
  }

  private boolean inputIsOk() {
    if(etLabel.getText().toString().compareTo("") == 0) {
      Toast.makeText(this, "Please don't use empty Label", Toast.LENGTH_SHORT).show();
      return false;
    }
    if(etInfo.getText().toString().compareTo("") == 0) {
      Toast.makeText(this, "Please don't use empty Information", Toast.LENGTH_SHORT).show();
      return false;
    }
    if(etLabel.getText().toString().contains(";")) {
      Toast.makeText(this, "Please don't use symbol ';' in Label field", Toast.LENGTH_SHORT).show();
      return false;
    }
    return true;
  }
}

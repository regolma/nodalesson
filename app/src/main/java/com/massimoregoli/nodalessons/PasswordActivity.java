package com.massimoregoli.nodalessons;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.massimoregoli.nodalessons.storage.FileData;

import java.io.IOException;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener {
  EditText etPassword1, etPassword2;
  Button btnOk, btnCancel;
  FileData fileData;
  boolean bFirstTime;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_password);
    etPassword1 = (EditText)findViewById(R.id.etPassword1);
    etPassword2 = (EditText)findViewById(R.id.etPassword2);
    btnOk = (Button)findViewById(R.id.btnOK);
    btnCancel = (Button)findViewById(R.id.btnCancel);

    btnOk.setOnClickListener(this);
    btnCancel.setOnClickListener(this);

    fileData = new FileData(this);
    bFirstTime = true;
    if(fileData.isPasswordPresent()) {
      bFirstTime = false;
      etPassword2.setVisibility(View.GONE);
    }
  }

  @Override
  public void onClick(View view) {
    if(view.getId() == btnCancel.getId()) {
      Intent intent = new Intent();
      setResult(RESULT_CANCELED, intent);
      finish();
    }
    if(view.getId() == btnOk.getId()) {
      if(bFirstTime) {
        if(etPassword2.getText().toString().compareTo(etPassword1.getText().toString()) == 0) {
          try {
            fileData.savePassword(etPassword1.getText().toString());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      } else {
        try {
          if (!fileData.checkPassword(etPassword1.getText().toString())) {
            return;
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      Intent intent = new Intent();
      intent.putExtra("password", etPassword1.getText().toString());
      setResult(RESULT_OK, intent);
      finish();
    }
  }
}

package com.massimoregoli.nodalessons.database;

import android.util.Base64;

import com.massimoregoli.nodalessons.util.Constants;

import java.util.Arrays;
import java.util.UUID;

import qp.qpk.Conjugate;
import qp.qpk.ModularMatrix;
import qp.qpk.PRNG2;
import qp.util.Util;

/**
 * Created by Massimo on 16/10/2016.
 */

public class CardInfo {
  private String publicInfo;
  private String label;
  private String secretInfo;
  private String clearInfo;
  private Boolean clear;

  public CardInfo(String mLine) {
    String[] fields = mLine.split(";");
    if (fields.length != 3)
      return;
    this.label = fields[0];
    this.secretInfo = fields[1];
    this.publicInfo = fields[2];
    this.clear = false;
  }

  public CardInfo(String label, String clearInfo, String password) {
    this.label = label;
    this.clearInfo = clearInfo;
    this.publicInfo =
            generateRandomKey(generateRandomPassword())
                    .getPublicKey()
                    .toString();
    Encode(password);
  }

  private String generateRandomPassword() {
    return UUID.randomUUID().toString();
  }

  private Conjugate generateRandomKey(String password) {
    Conjugate B = new Conjugate(Constants.DIMENSION,
            Constants.MODULUS);
    ModularMatrix Q = new ModularMatrix(Constants.DIMENSION,
            Constants.MODULUS);
    Q.fillWithCSV(Constants.Q);
    B.setQ(Q);
    ModularMatrix S = new ModularMatrix(Constants.DIMENSION,
            Constants.MODULUS);
    S.fillWithCSV(Constants.S);
    B.setNumberOfS(1);
    B.setS(S, 0);
    B.createPrivateKey(password);
    B.createPublicKey();
    return B;
  }

  private ModularMatrix fromCSV(String csv) {
    ModularMatrix mB = new ModularMatrix(Constants.DIMENSION, Constants.MODULUS);
    mB.fillWithCSV(csv);

    return mB;
  }

  private PRNG2 createQPDyn(String password) {
    ModularMatrix mB = fromCSV(this.publicInfo);
    Conjugate mA = generateRandomKey(password);
    mA.createSSK(mB);
    ModularMatrix SSK = mA.getSSK();
    PRNG2 code = new PRNG2();
    code.InitFromMatrix(SSK);
    return code;
  }

  private int nextMultipleOf4(int n) {
    int mul4 = n / 4;
    if ((mul4) * 4 != n * 4) {
      ++mul4;
    }
    return mul4;
  }

  private void Encode(String password) {
    PRNG2 code = createQPDyn(password);

    int intToBeProcessed =
            nextMultipleOf4(this.clearInfo.length());
    byte[] bCleanArray = Arrays.copyOf(this.clearInfo.getBytes(),
            intToBeProcessed * 4);
    long[] iClearArray = Util.byteArray2UIntArray(bCleanArray);
    long[] iEncArray = code.getBody(iClearArray,
            intToBeProcessed);
    byte[] bEncArray = Util.UIntArray2ByteArray(iEncArray);
    this.secretInfo = Base64.encodeToString(bEncArray, 0,
            this.clearInfo.length(), Base64.NO_WRAP);
  }


  public void Decode(String password) {
    PRNG2 code = createQPDyn(password);
    byte[] bEnc =
            Base64.decode(this.secretInfo, Base64.DEFAULT);
    int intToBeProcessed = nextMultipleOf4(bEnc.length);
    byte[] bEncArray = Arrays.copyOf(bEnc,
            intToBeProcessed * 4);
    long[] iEncArray = Util.byteArray2UIntArray(bEncArray);
    long[] iCleanArray = code.getBody(iEncArray,
            intToBeProcessed);
    byte[] bCleanArray = Util.UIntArray2ByteArray(iCleanArray);
    this.clearInfo = new String(Arrays.copyOf(bCleanArray,
            bEnc.length));
  }

  @Override
  public String toString() {
    return label;
  }

  public String getClearInfo() {
    return clearInfo;
  }

  public Boolean isClear() {
    return clear;
  }

  public void isClear(Boolean bClear) {
    this.clear = bClear;
  }

  String getLabel() {
    return label;
  }

  String getSecretInfo() {
    return secretInfo;
  }

  private String getPublicInfo() {
    return publicInfo;
  }

  public String getCSVInfo() {
    return label + ";" + getSecretInfo() + ";" + getPublicInfo();
  }
}

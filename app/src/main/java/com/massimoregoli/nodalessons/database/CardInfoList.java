package com.massimoregoli.nodalessons.database;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.massimoregoli.nodalessons.R;

import java.util.List;

/**
 * Created by Massimo on 16/10/2016.
 */

public class CardInfoList extends ArrayAdapter<CardInfo> {
  private List<CardInfo> mList;
  private int mResource;
  private LayoutInflater mInflater;

  public CardInfoList(Context context, int resource,
                      List<CardInfo> objects) {
    super(context, resource, objects);
    mList = objects;
    mResource = resource;
    mInflater =
            (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    if (convertView == null) {
      convertView = mInflater.inflate(mResource, null);
      new ViewHolder(convertView);
    }
    ViewHolder holder = (ViewHolder) convertView.getTag();
    CardInfo si = mList.get(position);
    holder.tvLabel.setText(si.getLabel());
    if(si.isClear()) {
      holder.tvInfo.setText(si.getClearInfo());
      holder.tvInfo.setTextColor(Color.RED);
    }
    else {
      holder.tvInfo.setText(si.getSecretInfo());
      holder.tvInfo.setTextColor(Color.BLACK);
    }
    return convertView;
  }

  private class ViewHolder {
    TextView tvLabel;
    TextView tvInfo;
    ViewHolder(View convertView) {
      tvLabel = (TextView)convertView.findViewById(R.id.tvLabel);
      tvInfo = (TextView)convertView.findViewById(R.id.tvInfo);
      convertView.setTag(this);
    }
  }
}

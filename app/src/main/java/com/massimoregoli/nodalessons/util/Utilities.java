package com.massimoregoli.nodalessons.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Massimo on 17/10/2016.
 */

public class Utilities {
  public static String getMD5EncryptedString(String encTarget){
    MessageDigest mdEnc;
    try {
      mdEnc = MessageDigest.getInstance("MD5");
      mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
      String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
      while ( md5.length() < 32 ) {
        md5 = "0"+md5;
      }
      return md5;
    } catch (NoSuchAlgorithmException e) {
      System.out.println("Exception while encrypting to md5");
      e.printStackTrace();
    } // Encryption algorithm
    return "";
  }
}

package com.massimoregoli.nodalessons;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.massimoregoli.nodalessons.database.CardInfo;
import com.massimoregoli.nodalessons.database.CardInfoList;
import com.massimoregoli.nodalessons.storage.FileData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
  private static final int ADD_NEW = 1;
  private static final int GET_PASSWORD = 2;
  private String password = "";
  private FileData fileList;
  private Button btnAdd, btnSave, btnReload;
  private ListView lvInfo;
  private List<CardInfo> si;
  private boolean changed = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    if(password.compareTo("") == 0) {
      askPassword();
    }
    setOnMe();

    loadList();
  }

  private void askPassword() {
    password = "forzaroma";
    Intent i = new Intent(this, PasswordActivity.class);
    startActivityForResult(i, GET_PASSWORD);
    Toast.makeText(this, "TODO:\nAdd Dialog to ask password.", Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onPause() {

    super.onPause();
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if(keyCode == KeyEvent.KEYCODE_BACK) {
      if(isChanged()) {
        askIfToSave(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            finish();
          }
        }, new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            try {
              fileList.save(lvInfo.getAdapter());
              isChanged(false);
            } catch (IOException e) {
              e.printStackTrace();
            }
            Toast.makeText(MainActivity.this,
                    String.format(Locale.getDefault(),
                            "Saved %d items.", lvInfo.getAdapter().getCount()),
                    Toast.LENGTH_SHORT).show();
            finish();

          }
        });
      }
      for(int i = 0; i < lvInfo.getAdapter().getCount(); i++)
        ((CardInfo)lvInfo.getAdapter().getItem(i)).isClear(false);
      lvInfo.setAdapter(lvInfo.getAdapter());
      lvInfo.postInvalidate();
    }
    return super.onKeyDown(keyCode, event);
  }

  private void askIfToSave(MaterialDialog.SingleButtonCallback negative,
                           MaterialDialog.SingleButtonCallback positive) {
    new MaterialDialog.Builder(this)
            .title(R.string.titleSaveBefore)
            .content(R.string.contentSaveBefore)
            .neutralText(android.R.string.cancel)
            .positiveText(android.R.string.yes)
            .negativeText(R.string.no)
            .onNegative(negative)
            .onPositive(positive)
            .show();

  }


  private void setOnMe() {
    btnAdd = (Button)findViewById(R.id.btnAdd);
    btnSave = (Button)findViewById(R.id.btnSave);
    btnReload = (Button)findViewById(R.id.btnReload);
    lvInfo = (ListView)findViewById(R.id.lvInfo);

    btnAdd.setOnClickListener(this);
    btnSave.setOnClickListener(this);
    btnReload.setOnClickListener(this);

    lvInfo.setOnItemClickListener(this);
    lvInfo.setOnItemLongClickListener(this);
  }

  private void loadList() {
    fileList = new FileData(this);
    try {
      si = fileList.read();
    } catch (IOException e) {
      e.printStackTrace();
    }
    if(si != null) {
      CardInfoList sil = new CardInfoList(this, R.layout.list_info, si);
      Toast.makeText(this,
              String.format(Locale.getDefault(), "%d items loaded.", si.size()),
              Toast.LENGTH_SHORT).show();
      lvInfo.setAdapter(sil);
      changed = false;
    }
  }

  @Override
  public void onClick(View view) {
    if(view.getId() == btnAdd.getId()) {
      Intent i = new Intent(this, AddInfoActivity.class);
      i.putExtra("password", password);
      startActivityForResult(i, ADD_NEW);
    }
    if(view.getId() == btnSave.getId()) {
      try {
        fileList.save(lvInfo.getAdapter());
        isChanged(false);
        Toast.makeText(this,
                String.format(Locale.getDefault(),
                        "Saved %d items.", lvInfo.getAdapter().getCount()),
                Toast.LENGTH_SHORT).show();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if(view.getId() == btnReload.getId()) {
      if(isChanged())
        askIfToSave(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            loadList();
          }
        }, new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            try {
              fileList.save(lvInfo.getAdapter());
              isChanged(false);
              loadList();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        });
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if(requestCode == ADD_NEW) {
      if (resultCode == Activity.RESULT_OK) {
        String info = data.getStringExtra("info");
        if (si == null)
          si = new ArrayList<>();
        si.add(new CardInfo(info));
        CardInfoList sil = new CardInfoList(this, R.layout.list_info, si);
        lvInfo.setAdapter(sil);
        lvInfo.postInvalidate();
        isChanged(true);
      }
    }
    if(requestCode == GET_PASSWORD) {
      if (resultCode == Activity.RESULT_OK) {
        password = data.getStringExtra("password");
      } else {
        finish();
      }
    }
  }

  private void isChanged(boolean b) {
    changed = b;
  }

  @Override
  public void onItemClick(AdapterView<?> adapterView,
                          View view, int i, long l) {
    CardInfo si = ((CardInfo)adapterView.getAdapter().getItem(i));

    si.isClear(!si.isClear());
    if(si.getClearInfo() == null
            || si.getClearInfo().compareTo("") == 0) {
      si.Decode(password);
    }
    lvInfo.setAdapter((CardInfoList)adapterView.getAdapter());
    lvInfo.postInvalidate();
  }

  @Override
  public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
    Toast.makeText(this, "TODO:\nDelete Entry", Toast.LENGTH_SHORT).show();
    return true;
  }

  public boolean isChanged() {
    return changed;
  }
}

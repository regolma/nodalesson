package com.massimoregoli.nodalessons.storage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ListAdapter;

import com.massimoregoli.nodalessons.database.CardInfo;
import com.massimoregoli.nodalessons.util.Utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


/**
 * Created by Massimo on 16/10/2016.
 */

public class FileData {
  private Context mContext;
  private static String FILENAME = "info.txt";
  private static String FILEPASSWORD = "password.txt";
  public FileData(Context context) {
    mContext = context;
  }

  public  boolean isPasswordPresent() {
    File file = mContext.getFileStreamPath(FILEPASSWORD);
    return !(file == null || !file.exists());
  }

  public boolean checkPassword(String password) throws IOException {
    BufferedReader reader = new BufferedReader(
            new InputStreamReader(mContext.openFileInput(FILEPASSWORD)));
    String mLine = reader.readLine();
    reader.close();
    String md5 = Utilities.getMD5EncryptedString(password);
    return md5.compareTo(mLine) == 0;
  }

  public void savePassword(String password) throws IOException {
    BufferedWriter writer =
            new BufferedWriter(new
                    OutputStreamWriter(mContext
                    .openFileOutput(FILEPASSWORD,0)));
    writer.write(Utilities.getMD5EncryptedString(password));
    writer.close();
  }

  @NonNull
  public ArrayList<CardInfo> read() throws IOException {
    ArrayList<CardInfo> lsi;
    BufferedReader reader = new BufferedReader(
            new InputStreamReader(mContext.openFileInput(FILENAME)));

    // do reading, usually loop until end of file reading
    lsi = new ArrayList<>();
    String mLine = reader.readLine();
    while (mLine != null) {
      CardInfo si = new CardInfo(mLine);
      lsi.add(si);
      mLine = reader.readLine();
    }
    reader.close();
    return lsi;
  }

  public void save(ListAdapter cardInfoList) throws IOException {
    if(cardInfoList == null)
      return;
    BufferedWriter writer =
            new BufferedWriter(
                    new OutputStreamWriter(mContext
                            .openFileOutput(FILENAME,0)));
    for(int i = 0; i < cardInfoList.getCount(); i++) {
      writer.write(((CardInfo)cardInfoList.getItem(i)).getCSVInfo() + "\n");
    }
    writer.close();
  }
}
